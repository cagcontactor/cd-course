package com.caglabs.goldenzone.infomodel.entity;

import org.junit.After;
import org.junit.Before;
import org.springframework.test.util.ReflectionTestUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Field;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;
import java.util.logging.Logger;

public abstract class AbstractEntityTestBase {
    private static Logger logger = Logger.getLogger(AbstractEntityTestBase.class.getName());

    private EntityManagerFactory emFactory;

    protected EntityManager entityManager;

    @Before
    public void setupEntityManager() throws ClassNotFoundException, SQLException {
        logger.info("Starting in-memory database for unit tests");
        Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        DriverManager.getConnection("jdbc:derby:memory:unit-testing-jpa;create=true").close();

        logger.info("Building JPA EntityManager for unit tests");
        emFactory = Persistence.createEntityManagerFactory("GoldenzonePU-test");
        entityManager = emFactory.createEntityManager();
    }

    @After
    public void tearDownEntityManager() throws Exception {
        logger.info("Shutting down Hibernate JPA layer.");
        if (entityManager != null) {
            entityManager.close();
        }
        if (emFactory != null) {
            emFactory.close();
        }
        logger.info("Stopping in-memory database.");
        try {
            DriverManager.getConnection("jdbc:derby:memory:unit-testing-jpa;drop=true").close();
        } catch (SQLNonTransientConnectionException ex) {
            if (ex.getErrorCode() != 45000) {
                throw ex;
            }
            // Shutdown success
        }
    }

    protected void injectEntityManager(Object o) {
        for (Field field : o.getClass().getDeclaredFields()) {
            if (field.getAnnotation(PersistenceContext.class) != null) {
                field.setAccessible(true);
                ReflectionTestUtils.setField(o, field.getName(), entityManager);
            }
        }
    }

    protected void withinTransaction(Runnable runnable) {
        entityManager.getTransaction().begin();
        try {
            runnable.run();
            entityManager.getTransaction().commit();
        } catch (Throwable t) {
            t.printStackTrace();
            entityManager.getTransaction().rollback();
        }

    }

}
