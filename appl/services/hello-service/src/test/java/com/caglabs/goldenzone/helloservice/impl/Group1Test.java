package com.caglabs.goldenzone.helloservice.impl;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class Group1Test {
    private HelloBean helloBean;

    @Before
    public void setup() {
        helloBean = new HelloBean();
    }

    @Test
    public void testGetMessages() {
        Map<Integer, String> messages = helloBean.getMessages();
        assertEquals(10, messages.size());
        String message = messages.get(1);
        assertEquals("Hello from group 1!", message);
    }
}