package com.caglabs.goldenzone.helloservice.impl;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class Group2Test {
    private HelloBean helloBean;

    @Before
    public void setup() {
        helloBean = new HelloBean();
    }

    @Test
    public void testGetMessages() {
        Map<Integer, String> messages = helloBean.getMessages();
        assertEquals(10, messages.size());
        String message = messages.get(2);
        assertEquals("Hello change 2 from group 2!", message);
    }
}
