package com.caglabs.goldenzone.helloservice.client;

import java.util.Map;

public interface Hello {
    public static class HelloException extends Exception {
        public HelloException(String message) {
            super(message);
        }

        public HelloException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    String sayHello(String name) throws HelloException;

	Map<Integer, String> getMessages();
}