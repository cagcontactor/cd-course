package com.caglabs.goldenzone.helloservice.impl;

import com.caglabs.goldenzone.helloservice.client.Hello;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUser;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUserDao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Simple test bean.
 */
@Stateless
@Remote(Hello.class)
public class HelloBean implements Hello {
    private Logger logger = Logger.getLogger(HelloBean.class.getName());

    @Inject
    private GoldenzoneUserDao goldenzoneUserDao;

    @PersistenceContext(unitName = "GoldenzonePU")
    private EntityManager entityManager;

    @Override
    public String sayHello(String name) throws HelloException {
        if (goldenzoneUserDao.entityManager == null) {
            goldenzoneUserDao.entityManager = entityManager;
        }
        GoldenzoneUser user = goldenzoneUserDao.getSystemUserByName(name);
        if (user != null) {
            String message = "Hello " + user.getDisplayName() + " (id=" + user.getId() + ")!!! The time is " +
                    new SimpleDateFormat("HH:mm:ss").format(new Date());
            logger.info(message);
            return message;
        } else {
            String message = "Unknown user: " + name;
            logger.info(message);
            throw new HelloException(message);
        }
    }

	@Override
	public Map<Integer, String> getMessages() {
        Map<Integer, String> result = new HashMap<>();
        for (int i = 1; i <= 10; i++) {
            result.put(i, ResourceBundle.getBundle("group"+i).getString("message"));
        }
        return result;
	}
}
