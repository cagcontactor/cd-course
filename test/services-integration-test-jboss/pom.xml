<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <artifactId>test</artifactId>
        <groupId>com.caglabs.goldenzone</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>

    <artifactId>services-integration-test-jboss</artifactId>
    <description>Services Integration Test, automated integration tests of the services running on JBoss</description>
    <packaging>jar</packaging>

    <properties>
        <jboss.version>7.1.1.Final</jboss.version>
        <server.location>${project.build.directory}/server</server.location>
        <servlet.port>18080</servlet.port>
        <management.port>19999</management.port>
        <remoting.transport.port>14447</remoting.transport.port>
        <jrmp.port>11090</jrmp.port>
        <jmx.port>11091</jmx.port>
        <rmi.port>11099</rmi.port>
    </properties>

    <dependencies>
        <!-- Internal -->
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>infomodel</artifactId>
            <version>${project.version}</version>
        </dependency>

        <dependency>	
            <groupId>${project.parent.groupId}</groupId>
            <artifactId>app-ear</artifactId>
            <version>${project.parent.version}</version>
            <type>ear</type>
        </dependency>
        <dependency>
            <groupId>${project.parent.groupId}</groupId>
            <artifactId>hello-service</artifactId>
            <version>${project.parent.version}</version>
            <type>ejb</type>
        </dependency>

        <dependency>
            <groupId>${project.parent.groupId}</groupId>
            <artifactId>jboss-config</artifactId>
            <version>${project.parent.version}</version>
            <type>zip</type>
            <classifier>test</classifier>
        </dependency>
       <dependency>
           <groupId>${project.parent.groupId}</groupId>
           <artifactId>db-loader</artifactId>
           <version>${project.parent.version}</version>
       </dependency>

        <!-- Import the transaction spec API, we use runtime scope because we aren't using any direct
              reference to the spec API in our client code -->
        <dependency>
            <groupId>org.jboss.spec.javax.transaction</groupId>
            <artifactId>jboss-transaction-api_1.1_spec</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- Import the EJB 3.1 API, we use runtime scope because we aren't using any direct
         reference to EJB spec API in our client code -->
        <dependency>
            <groupId>org.jboss.spec.javax.ejb</groupId>
            <artifactId>jboss-ejb-api_3.1_spec</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- JBoss EJB client API jar. We use runtime scope because the EJB client API
         isn't directly used in this example. We just need it in our runtime classpath -->
        <dependency>
            <groupId>org.jboss</groupId>
            <artifactId>jboss-ejb-client</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- client communications with the server use XNIO -->
        <dependency>
            <groupId>org.jboss.xnio</groupId>
            <artifactId>xnio-api</artifactId>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>org.jboss.xnio</groupId>
            <artifactId>xnio-nio</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- The client needs JBoss remoting to access the server -->
        <dependency>
            <groupId>org.jboss.remoting3</groupId>
            <artifactId>jboss-remoting</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- Remote EJB accesses can be secured -->
        <dependency>
            <groupId>org.jboss.sasl</groupId>
            <artifactId>jboss-sasl</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- data serialization for invoking remote EJBs -->
        <dependency>
            <groupId>org.jboss.marshalling</groupId>
            <artifactId>jboss-marshalling-river</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- Test -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
        </dependency>

        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
        </dependency>
    </dependencies>

    <build>
        <testResources>
            <testResource>
                <directory>${project.build.testSourceDirectory}/../resources</directory>
                <filtering>true</filtering>
            </testResource>
        </testResources>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>unpack</id>
                        <phase>process-test-resources</phase>
                        <goals>
                            <goal>unpack</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>org.jboss.as</groupId>
                                    <artifactId>jboss-as-dist</artifactId>
                                    <version>${jboss.version}</version>
                                    <type>zip</type>
                                    <overWrite>false</overWrite>
                                    <outputDirectory>${server.location}</outputDirectory>
                                </artifactItem>
                                <artifactItem>
                                    <groupId>${project.parent.groupId}</groupId>
                                    <artifactId>jboss-config</artifactId>
                                    <version>${project.parent.version}</version>
                                    <classifier>test</classifier>
                                    <!-- test or prod -->
                                    <type>zip</type>
                                    <overWrite>false</overWrite>
                                    <outputDirectory>${server.location}/jboss-as-${jboss.version}</outputDirectory>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.cargo</groupId>
                <artifactId>cargo-maven2-plugin</artifactId>
                <version>1.3.2</version>
                <configuration>
                    <wait>true</wait>
                    <container>
                        <containerId>jboss71x</containerId>
                        <home>${server.location}/jboss-as-${jboss.version}</home>
                    </container>
                    <deployables>
                        <deployable>
                            <groupId>${project.parent.groupId}</groupId>
                            <artifactId>app-ear</artifactId>
                            <type>ear</type>
                            <!-- need to increase the timeout (default is 20 s) for deploying since it takes a while -->
                            <pingURL>http://localhost:${servlet.port}/manager-ui</pingURL>
                            <pingTimeout>180000</pingTimeout>
                        </deployable>
                    </deployables>
                    <configuration>
                        <properties>
                            <cargo.servlet.port>${servlet.port}</cargo.servlet.port>
                            <cargo.jboss.management.port>${management.port}</cargo.jboss.management.port>
                            <cargo.jboss.remoting.transport.port>${remoting.transport.port}</cargo.jboss.remoting.transport.port>
                            <cargo.jboss.jrmp.port>${jrmp.port}</cargo.jboss.jrmp.port>
                            <cargo.jboss.jmx.port>${jmx.port}</cargo.jboss.jmx.port>
                            <cargo.rmi.port>${rmi.port}</cargo.rmi.port>
                        </properties>
                    </configuration>
                </configuration>
            </plugin>
        </plugins>
    </build>
    <profiles>
        <profile>
            <id>int-test</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-failsafe-plugin</artifactId>
                        <version>2.13</version>
                        <executions>
                            <execution>
                                <id>run-integration-test</id>
                                <phase>integration-test</phase>
                                <goals>
                                    <goal>integration-test</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>verify-test-results</id>
                                <phase>post-integration-test</phase>
                                <goals>
                                    <goal>verify</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.codehaus.cargo</groupId>
                        <artifactId>cargo-maven2-plugin</artifactId>
                        <configuration>
                            <wait>false</wait>
                        </configuration>
                        <executions>
                            <execution>
                                <id>start-server</id>
                                <phase>pre-integration-test</phase>
                                <goals>
                                    <goal>start</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>stop-server</id>
                                <phase>post-integration-test</phase>
                                <goals>
                                    <goal>stop</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>