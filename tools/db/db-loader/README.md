DB loader
=========
This module handles loading of information model example data into the database.

It is implemented as a unit test case and is run:

    $ mvn clean compile exec:java